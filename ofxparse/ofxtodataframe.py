from ofxparse import OfxParser
import pandas as pd
import codecs
import os.path as path
import sys, warnings
import decimal

# fields of transactions are auto extracted using dir(transactiontype)-{attributes starting with '_'}

def ofx_to_dataframe(fileobjs, id_len=24):
    data = {}
    assert(isinstance(fileobjs, list))
    for fileobj in fileobjs:
        
        #with codecs.open(fname) as fileobj:
        #    ofx = OfxParser.parse(fileobj)
        ofx = OfxParser.parse(fileobj)
        # it seems one ofx file contains only one securities list. Create a mapping from ID to ticker
        security_map = {}
        if hasattr(ofx, 'security_list') and ofx.security_list:
            security_map.update({x.uniqueid : x.ticker for x in ofx.security_list})
        # different transaction types have different fields. So we create df for each txn_type
        # and append the contents of each txn into appropriate df
        # use of vars() to get __dict__ hash is based of https://www.askpython.com/python/built-in-methods/python-vars-method
        # the __dict__ attribute of obj contains all the attributes of the object which can be writable.
        for account in ofx.accounts:
            for txn_type in [('transactions', "Transaction"),
                             ('transactions', "InvestmentTransaction"),
                             ('positions', "Position")]:
                if hasattr(account.statement, txn_type[0]) and len(
                        getattr(account.statement, txn_type[0])):
                    df = pd.DataFrame(
                        [vars(tr) for tr in getattr(account.statement, txn_type[0])
                         if type(tr).__name__ == txn_type[1]])
                    if len(df):
                        df['acctnum'] = account.number
                        if txn_type[1] in data:
                            data[txn_type[1]] = pd.concat([data[txn_type[1]], df], ignore_index=True)
                        else:
                            data[txn_type[1]] = df

            # add cash balance as a "Cash" position
            cash_amount = None
            if hasattr(account.statement, 'balance'):
                cash_amount = account.statement.balance
                dt = account.statement.balance_date
            elif hasattr(account.statement, 'available_cash'):
                cash_amount = account.statement.available_cash
                dt = account.statement.end_date

            if cash_amount is not None:
                df1 = data.get('Position',
                              pd.DataFrame(columns=[
                                  'date', 'market_value', 'security', 'unit_price', 'units', 'acctnum']))
                df2 = pd.DataFrame({
                    'date'        : dt,
                    'security'    : account.statement.currency.upper(),
                    'market_value': cash_amount,
                    'units'       : cash_amount,
                    'unit_price'  : decimal.Decimal('1.00'),
                    'acctnum'     : account.number}, index=[0])
                # have to convert df2.dtypes to match with that of df1, using df2.astype() because
                # df1['date'] dtype was coming out to be datetime64[ns] vs df2 was datetime64[us]
                # ie. nano vs micro sec
                # was causing this error
                data['Position'] = pd.concat([df1, df2.astype(df1.dtypes)], ignore_index=True)
            
    # add fname info into each df. Truncate ID if needed
    for key,df in data.items():
        df['fname'] = hasattr(fileobj, 'name') and fileobj.name or 'stdin'
        if 'id' in df.columns:
            df['id'] = df['id'].str[:id_len]  # clip the last part of the ID which changes from download to download
        if 'security' in df.columns:
            df['security'] = df['security'].apply(lambda x: security_map.get(x, x))
        if 'AGGREGATE_TYPES' in df.columns :
            del df['AGGREGATE_TYPES']
    return data

__dev_notes__='''
For brokerage, balances are available in account.statement.balance_list... but overall cash is also summarized in account.statement.available_cash corresponding to statement.end_date
For bank, balance is available in account.statement.balance (and balance_date)

'''
